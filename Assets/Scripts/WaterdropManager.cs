using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterdropManager : MonoBehaviour
{
    public float xMin = -38f, xMax = -3.5f, zMin = -10f, zMax = 25f;
    public float yMin = 30f, yMax = 40f;
    public float timeInterval = 5f;
    GameObject waterdropPrefab;
    GameObject waterdropPrefab2;

    void Start()
    {
        waterdropPrefab = Resources.Load<GameObject>("Waterdrop");
        waterdropPrefab2 = Resources.Load<GameObject>("Waterdrop2");
        StartCoroutine(DroppingCoroutine());
        StartCoroutine(DroppingCoroutine2());
    }

    IEnumerator DroppingCoroutine()
    {
        while(true)
        {
            yield return new WaitForSeconds(timeInterval * Random.Range(0.1f, 1.9f));
            Vector3 position = new Vector3(Random.Range(xMin, xMax), Random.Range(yMin, yMax), Random.Range(zMin, zMax));
            GameObject instance = GameObject.Instantiate(waterdropPrefab, position, Quaternion.identity);
            float scale = instance.transform.localScale.x * Random.Range(0.8f, 1.0f);
            instance.transform.localScale = new Vector3(scale, scale, scale);
        }
    }

    IEnumerator DroppingCoroutine2()
    {
        float xMid = (xMax + xMin) * 0.5f;
        float xRad = (xMax - xMin) * 0.5f;
        float zMid = (zMax + zMin) * 0.5f;
        float zRad = (zMax - zMin) * 0.5f;

        xRad *= 2;
        zRad *= 2;
        float _xMin = xMid - xRad;
        float _xMax = xMid + xRad;
        float _zMin = zMid - zRad;
        float _zMax = zMid + zRad;

        Debug.Log(_xMin);
        Debug.Log(_xMax);
        Debug.Log(_zMin);
        Debug.Log(_zMax);

        while(true)
        {
            yield return new WaitForSeconds(timeInterval * Random.Range(0.1f, 1.9f) / 8);
            Vector3 position = Vector3.zero;
            do
                position = new Vector3(Random.Range(_xMin, _xMax), Random.Range(yMin, yMax), Random.Range(_zMin, _zMax));
            while(xMin <= position.x && position.x <= xMax && zMin <= position.z && position.z <= zMax); 
            GameObject instance = GameObject.Instantiate(waterdropPrefab2, position, Quaternion.identity);
            float scale = instance.transform.localScale.x * Random.Range(0.5f, 0.9f);
            instance.transform.localScale = new Vector3(scale, scale, scale);
        }
    }
}
