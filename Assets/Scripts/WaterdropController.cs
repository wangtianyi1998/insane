using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterdropController : MonoBehaviour
{
    GameObject waterdropPrefab;
    public int cnt = 2;

    void Start()
    {
        waterdropPrefab = Resources.Load<GameObject>("Waterdrop");
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Plat")
        {
            other.gameObject.GetComponent<FallPlat>().ChangeState(0);
        }
        else if(other.gameObject.tag == "Player1" || other.gameObject.tag == "Player2")
        {
            Vector3 hitDir = other.gameObject.transform.position - transform.position;
            if(hitDir.y < 0f)
                hitDir.y = 0f;
            hitDir.Normalize();
			other.gameObject.GetComponent<CharacterControls>().HitPlayer(hitDir * transform.localScale.x * 14f, 0f);
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if(cnt >= 1)
        {
            int total = Random.Range(2, 6);
            for(int i = 0; i < total; i++)
            {
                Vector3 deltaDirection = Random.onUnitSphere;
                if(deltaDirection.y < 0)
                    deltaDirection.y = -deltaDirection.y;
                Vector3 position = transform.position + deltaDirection * 1f;
                GameObject instance = Instantiate(waterdropPrefab, position, Quaternion.identity);
                instance.GetComponent<WaterdropController>().cnt = cnt - 1;
                instance.GetComponent<Rigidbody>().AddForce(deltaDirection * 5f * cnt, ForceMode.Impulse);
                float scale = gameObject.transform.localScale.x * (cnt == 2? Random.Range(0.5f, 0.6f): Random.Range(0.2f, 0.4f));
                instance.transform.localScale = new Vector3(scale, scale, scale);
                if(cnt == 1)
                    Destroy(instance.GetComponent<SphereCollider>());
            }
        }
        Destroy(gameObject);
    }
    void FixedUpdate()
    {
        GetComponent<Rigidbody>().AddForce(Vector3.down * 10f, ForceMode.Force);
    }

    void Update()
    {
        if(transform.position.y < -40f)
            Destroy(gameObject);
    }
}
