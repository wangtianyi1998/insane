using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scoreboard : MonoBehaviour
{
    static public Scoreboard singleton;

    static public Dictionary<GameObject, int> state = new Dictionary<GameObject, int>();
    static public int totalCount;
    static public int[] playerScore = new int[3];
    static public int[] playerScore2 = new int[3];

    public GameObject player1ui;

    static int[] killCount = new int[3];

    public TextMesh[] killCounts;

    void Awake()
    {
        singleton = this;

        state = new Dictionary<GameObject, int>();
        totalCount = 0;
        playerScore = new int[3];
        killCount = new int[3];
    }

    void Update()
    {
        playerScore[1] = playerScore[2] = 0;
        foreach(KeyValuePair<GameObject, int> kvp in state)
        {
            if(kvp.Key.GetComponent<FallPlat>().changing > 0)
                continue;
            int _state = kvp.Key.GetComponent<FallPlat>().state;
            if(_state != 0)
                playerScore[_state]++;
        }
        RefreshUIs();
    }

    static public void Register(GameObject obj)
    {
        state[obj] = 0;
        totalCount++;
    }
    
    void RefreshUIs()
    {
        Debug.Log(playerScore[1].ToString() + " " + playerScore[2].ToString());
        float ratio = 0;
        if(playerScore[1] == 0 && playerScore[2] == 0)
            ratio = 0.5f;
        else
            ratio = (float)playerScore[2] / (playerScore[1] + playerScore[2]);
        player1ui.transform.localScale = new Vector3(1f, ratio, 1f);
    
        transform.rotation = Quaternion.Euler(Vector3.Slerp(new Vector3(0f, 0f, 91.6f), new Vector3(0f, 0f, 88.4f), 1 - ratio));

        for(int i = 0; i < 4; i++)
            killCounts[i].text = "◆ " + playerScore[i % 2 + 1].ToString();
    }

    static public void KillCount(int player)
    {
        killCount[player]--;
        singleton.RefreshUIs();
    }

    static public int GetResult()
    {
        if(playerScore[1] > playerScore[2])
            return 1;
        else if(playerScore[2] > playerScore[1])
            return 2;
        else
            return 0;
    }
}
