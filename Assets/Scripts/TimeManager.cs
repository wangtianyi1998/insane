using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TimeManager : MonoBehaviour
{
    public int t;

    public TextMesh leftTime;
    public TextMesh[] gameResult;

    void Start()
    {
        Time.timeScale = 1;
        StartCoroutine(TimeCoroutine());
    }

    IEnumerator TimeCoroutine()
    {
        RefreshTime();
        while(t > 0)
        {
            yield return new WaitForSeconds(1f);
            --t;
            RefreshTime();
        }
        int result = Scoreboard.GetResult();
        gameResult[0].gameObject.SetActive(true);
        gameResult[1].gameObject.SetActive(true);
        switch(result)
        {
            case 0:
                gameResult[0].text = "Tie.";
                gameResult[0].color = Color.white;
                break;
            case 1:
                gameResult[0].text = "Player1 wins!";
                gameResult[0].color = new Color(0.5229619f, 0.8525299f, 0.9811321f, 1f);
                break;
            case 2:
                gameResult[0].text = "Player2 wins!";
                gameResult[0].color = new Color(0.3948915f, 0.9622642f, 0.4747094f, 1f);
                break;
        }
        gameResult[1].text = gameResult[0].text;
        gameResult[2].gameObject.SetActive(true);
        gameResult[3].gameObject.SetActive(true);
        Time.timeScale = 0;
        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.R));
        SceneManager.LoadScene(0);
    }
    void RefreshTime()
    {
        Debug.Log(t);
        leftTime.text = (t / 60).ToString("D2") + ":" + (t % 60).ToString("D2");
    }

}
