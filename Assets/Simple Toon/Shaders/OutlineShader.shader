Shader "Simple Toon/Outline Shader"
{
    Properties
    {
        _OtlColor ("Color", Color) = (0, 0, 0, 1)
    }
    SubShader
    {
        Pass
        {
			Tags { "RenderType" = "Opaque" "LightMode" = "ForwardBase"  }
			Blend Off
            Cull Front

            CGPROGRAM
            #pragma vertex vert
 			#pragma fragment frag

			#include "UnityCG.cginc"
            //#include "STCore.cginc"

			float4 _OtlColor;

            struct appdata
            {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
			};

            v2f vert (appdata v)
            {
                v2f o;
			    o.pos = UnityObjectToClipPos(v.vertex);
			    return o;
            }

            fixed4 frag(v2f i) : SV_Target
			{
				//clip(-negz(_OtlWidth));
		    	return _OtlColor;
			}

            ENDCG
        }
    }
}
