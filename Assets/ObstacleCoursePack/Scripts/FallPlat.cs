﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FallPlat : MonoBehaviour
{
	public float fallTime = 0.5f;

	Material selfMaterial;

	public int state = 0;
	//0-中立 1-玩家1 2-玩家2
	public int changing = 0;

	float originY;

	void Start()
	{
		selfMaterial = new Material(GetComponent<Renderer>().material);
		GetComponent<Renderer>().material = selfMaterial;
		originY = transform.position.y;
		Scoreboard.Register(gameObject);
	}

	void OnCollisionEnter(Collision collision)
	{
		if(changing > 0)
			return;
		foreach (ContactPoint contact in collision.contacts)
		{
			//Debug.DrawRay(contact.point, contact.normal, Color.white);
			if (collision.gameObject.tag == "Player1")
			{
				if(state == 2)
				{
					//collision.gameObject.GetComponent<CharacterControls>().GetScoreRate();
					StartCoroutine(Fall(fallTime));
				}
				else if(ChangeState(1))
					collision.gameObject.GetComponent<CharacterControls>().GetScoreRate();
			}
			if (collision.gameObject.tag == "Player2")
			{
				if(state == 1)
				{
					//collision.gameObject.GetComponent<CharacterControls>().GetScoreRate();
					StartCoroutine(Fall(fallTime));
				}
				else if(ChangeState(2))
					collision.gameObject.GetComponent<CharacterControls>().GetScoreRate();
			}
		}
	}
	
	IEnumerator Fall(float time)
	{
		Color fadeColor = new Color(0.1525899f, 0.2361484f, 0.254717f);

		changing++;
		Color origin = selfMaterial.GetColor("_Color");
		for(float t = 0; t < time; t += Time.deltaTime)
		{
			float x = t / time;
			x = Mathf.Pow(x, 2);
			Color c = origin * (1 - x) + fadeColor * x;
			selfMaterial.SetColor("_Color", c);
			yield return 0;
		}
		GetComponent<BoxCollider>().enabled = false;
		GetComponent<MeshRenderer>().enabled = false;
		yield return new WaitForSeconds(5f);
		
		transform.position += new Vector3(0f, 10f - originY, 0f);
		GetComponent<BoxCollider>().enabled = true;
		GetComponent<MeshRenderer>().enabled = true;
		transform.DOMoveY(originY, 0.5f);
		changing--;
		ChangeState(0);
	}

	public bool ChangeState(int state)
	{
		if(changing > 0)
			return false;
		this.state = state;
		Color color = Color.white;
		switch(state)
		{
			case 0:
				color = new Color(0.8490566f, 0.8490566f, 0.8490566f, 1f);
				break;
			case 1:
				color = new Color(0.5229619f, 0.8525299f, 0.9811321f, 1f);
				break;
			case 2:
				color = new Color(0.3948915f, 0.9622642f, 0.4747094f, 1f);
				break;
		}
		selfMaterial.SetColor("_Color", color);
		return true;
	}
}
