﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {

	public float followSpeed = 3; //Speed ​​at which the camera follows us
	public float mouseSpeed = 2; //Speed ​​at which we rotate the camera with the mouse
	//public float controllerSpeed = 5; //Speed ​​at which we rotate the camera with the joystick
	public float cameraDist = 3; //Distance to which the camera is located

	public Transform[] targets; //Player the camera follows

	[HideInInspector]
	public Transform pivot; //Pivot on which the camera rotates(distance that we want between the camera and our character)
	[HideInInspector]
	public Transform camTrans; //Camera position

	float turnSmoothing = .1f; //Smooths all camera movements (Time it takes the camera to reach the rotation indicated with the joystick)
	public float minAngle = -35; //Minimum angle that we allow the camera to reach
	public float maxAngle = 35; //Maximum angle that we allow the camera to reach

	float smoothX;
	float smoothY;
	float smoothXvelocity;
	float smoothYvelocity;
	public float lookAngle; //Angle the camera has on the Y axis
	public float tiltAngle; //Angle the camera has up / down

	public void Init()
	{
		//camTrans = Camera.main.transform;
		camTrans = GetComponentInChildren<Camera>().transform;
		pivot = camTrans.parent;
	}

	void FollowTargets(float d)
	{ //Function that makes the camera follow the player
		Vector3 avgPosition = Vector3.zero;
		foreach(Transform _transform in targets)
			avgPosition += _transform.position;
		avgPosition /= targets.Length;
		avgPosition.y = 0f;
		float speed = d * followSpeed; //Set speed regardless of fps
		Vector3 targetPosition = Vector3.Lerp(transform.position, avgPosition, speed); //Bring the camera closer to the player interpolating with the velocity(0.5 half, 1 everything)
		transform.position = targetPosition; //Update the camera position
	}

	private void Update()
	{
		FollowTargets(Time.deltaTime); //Follow player
	}

	private void LateUpdate()
	{
		//Here begins the code that is responsible for bringing the camera closer by detecting wall
		float dist = cameraDist + 1.0f; // distance to the camera + 1.0 so the camera doesnt jump 1 unit in if it hits someting far out
		Ray ray = new Ray(camTrans.parent.position, camTrans.position - camTrans.parent.position);// get a ray in space from the target to the camera.
		RaycastHit hit;
		// read from the taret to the targetPosition;
		if (Physics.Raycast(ray, out hit, dist))
		{
			if (hit.transform.tag == "Wall")
			{
				// store the distance;
				dist = hit.distance - 0.25f;
			}
		}
		// check if the distance is greater than the max camera distance;
		if (dist > cameraDist) dist = cameraDist;
//		camTrans.localPosition = new Vector3(0, 0, -dist);
	}

	//public static CameraManager singleton; //You can call CameraManager.singleton from other script (There can be only one)
	void Awake()
	{
		//singleton = this; //Self-assigns
		Init();
	}

}
